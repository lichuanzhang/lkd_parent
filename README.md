<p align="center">
 <img src="https://img.shields.io/badge/lkd-success.svg" alt="Build Status">
 <img src="https://img.shields.io/badge/Spring%20Cloud-2020-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-2.3-blue.svg" alt="Downloads">
</p>

```
lkd_parent
└── lkd_common 公共方法模块
└── lkd_service 服务模块
    └── lkd_gate_way 网关
    └── lkd_gateway_microapp 小程序网关
    └── lkd_microapp 小程序服务
    └── lkd_order_service 订单服务
    └── lkd_production_service 工单服务
    └── lkd_status_service 状态服务
    └── lkd_user_service 用户服务
    └── lkd_vms_service 售货机服务
    └── service_common 微服务公共模块
```
